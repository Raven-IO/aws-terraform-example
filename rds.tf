module "postgres" {
  source                  = "./rds_pg"
  allowed_cidr_blocks     = var.allowed_cidr_blocks
  allowed_security_groups = concat(var.allowed_security_groups, [module.lambda_s3_data_bucket.lambda_sg_id])
  backup_retention_period = 7
  db_subnet_group_name    = var.db_subnet_group_name
  name                    = "${var.prefix}-customers"
  vpc_id                  = var.vpc_id
  additional_tags         = var.additional_tags
}
