module "s3_data_bucket" {
  source                = "./s3"
  bucket_name           = "${var.prefix}-nodes-data"
  acl                   = "private"
  lambda_arn            = module.lambda_s3_data_bucket.lambda_arn
  create_lambda_handler = true
  additional_tags       = var.additional_tags
  delete_after_14_days  = true
}

module "s3_persist_bucket" {
  source          = "./s3"
  bucket_name     = "${var.prefix}-persist-data"
  acl             = "private"
  additional_tags = var.additional_tags
}

module "s3_storage_bucket" {
  source          = "./s3"
  bucket_name     = "${var.prefix}-storage-data"
  acl             = "private"
  additional_tags = var.additional_tags
}

module "s3_symbols_bucket" {
  source          = "./s3"
  bucket_name     = "${var.prefix}-symbols-metadata"
  acl             = "private"
  additional_tags = var.additional_tags
}

module "s3_traces_bucket" {
  source          = "./s3"
  bucket_name     = "${var.prefix}-traces-data"
  acl             = "private"
  additional_tags = var.additional_tags
}
