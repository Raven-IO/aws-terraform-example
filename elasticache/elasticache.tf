resource "aws_security_group" "redis" {
  name        = "${var.name}_redis"
  description = "Redis security group for ${var.name}"
  vpc_id      = var.vpc_id
  tags = merge(var.additional_tags, {
    Name     = "${var.name}_redis"
    STATEFUL = "true"
  })
}

resource "aws_security_group_rule" "redis_ingress_sgs" {
  count                    = length(var.allowed_security_groups)
  from_port                = var.port
  protocol                 = "tcp"
  security_group_id        = aws_security_group.redis.id
  to_port                  = var.port
  type                     = "ingress"
  source_security_group_id = var.allowed_security_groups[count.index]
}

resource "aws_security_group_rule" "redis_ingress_cidrs" {
  from_port         = var.port
  protocol          = "tcp"
  security_group_id = aws_security_group.redis.id
  to_port           = var.port
  type              = "ingress"
  cidr_blocks       = var.allowed_cidr_blocks
}

resource "aws_security_group_rule" "redis_egress" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.redis.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = var.allowed_cidr_blocks
}

resource "aws_elasticache_cluster" "this" {
  cluster_id               = replace(var.name, "_", "-")
  engine                   = "redis"
  node_type                = var.node_type
  engine_version           = "6.2"
  num_cache_nodes          = 1
  port                     = var.port
  apply_immediately        = true
  az_mode                  = "single-az"
  parameter_group_name     = "default.redis6.x"
  subnet_group_name        = var.redis_subnet_group_name
  security_group_ids       = [aws_security_group.redis.id]
  tags                     = var.additional_tags
  snapshot_retention_limit = 5
}
