variable "name" {
  type        = string
  description = "The name of the Elasticache instance or prefix for the resources."
}

variable "vpc_id" {
  type        = string
  description = "The VPC ID to deploy the resources."
}

variable "allowed_security_groups" {
  type        = list(string)
  description = "The security groups that are allowed to access the Elasticache (Redis) instance."
}

variable "node_type" {
  type        = string
  description = "The node type to use for the Elasticache (Redis) instance."
}

variable "redis_subnet_group_name" {
  type        = string
  description = "The name of the Elasticache subnet group to use for the Elasticache (Redis) instance."
}

variable "allowed_cidr_blocks" {
  type        = list(string)
  description = "The CIDR blocks that are allowed to access the Elasticache (Redis) instance."
}

variable "additional_tags" {
  description = "(Optional) Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  type        = map(string)
  default     = {}
}

variable "port" {
  default     = 6379
  description = "The port on which the Redis server listens."
  type        = string
}
