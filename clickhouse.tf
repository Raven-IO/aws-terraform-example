module "clickhouse" {
  source               = "./ec2"
  ami                  = "ami-014c4c01bcb1eacc5"
  name                 = "${var.prefix}-clickhouse"
  subnet_id            = var.private_subnets[0]
  vpc_id               = var.vpc_id
  instance_type        = "r5.4xlarge"
  iam_instance_profile = aws_iam_instance_profile.clickhouse_instance_profile.name

  whitelisted_cidr_blocks = var.allowed_cidr_blocks

  whitelisted_tcp_ports = [
    22,   # SSH
    9000, # Clickhouse TCP
  ]
}

resource "aws_iam_instance_profile" "clickhouse_instance_profile" {
  name = "clickhouse-instance-profile"
  role = aws_iam_role.clickhouse_instance_role.name
}

resource "aws_iam_role" "clickhouse_instance_role" {
  name = "clickhouse-instance-role"

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      },
    ]
  })
}
