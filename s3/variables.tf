variable "bucket_name" {
  type        = string
  description = "The name of the S3 bucket."
}

# [private public-read public-read-write authenticated-read aws-exec-read log-delivery-write]
variable "acl" {
  type        = string
  default     = "private"
  description = "The canned ACL to apply. Defaults to 'private'."
}

variable "lambda_arn" {
  default     = ""
  description = "The ARN of the Lambda function to invoke when an object is created in the bucket."
}

variable "create_lambda_handler" {
  default     = false
  description = "Flag to conditionally create a Lambda function to handle S3 events."
}

variable "additional_tags" {
  description = "(Optional) Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  type        = map(string)
  default     = {}
}

variable "delete_after_14_days" {
  description = "Flag to conditionally delete objects after 14 days"
  type        = bool
  default     = false
}
