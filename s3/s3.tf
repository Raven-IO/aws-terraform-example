resource "aws_s3_bucket" "this" {
  bucket = var.bucket_name
  tags = merge(var.additional_tags, {
    STATEFUL       = "true"
    BACKUP_ENABLED = "true"
  })
}

resource "aws_lambda_permission" "this" {
  count         = var.create_lambda_handler ? 1 : 0
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = var.lambda_arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.this.arn
}

resource "aws_s3_bucket_notification" "this" {
  count  = var.create_lambda_handler ? 1 : 0
  bucket = aws_s3_bucket.this.id

  lambda_function {
    lambda_function_arn = var.lambda_arn
    events              = ["s3:ObjectCreated:*"]
  }
}
resource "aws_s3_bucket_acl" "this" {
  count  = var.acl != "private" ? 1 : 0
  bucket = aws_s3_bucket.this.id
  acl    = var.acl
}

resource "aws_s3_bucket_server_side_encryption_configuration" "this" {
  bucket = aws_s3_bucket.this.id

  rule {
    apply_server_side_encryption_by_default {
      sse_algorithm = "AES256"
    }
  }
}

resource "aws_s3_bucket_versioning" "this" {
  bucket = aws_s3_bucket.this.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "this" {
  # Must have bucket versioning enabled first
  depends_on = [aws_s3_bucket_versioning.this]

  bucket = aws_s3_bucket.this.id

  rule {
    id     = "config"
    status = "Enabled"

    dynamic "expiration" {
      for_each = var.delete_after_14_days ? [1] : []
      content {
        days = 14
      }
    }

    dynamic "noncurrent_version_transition" {
      for_each = var.delete_after_14_days ? [] : [1]
      content {
        noncurrent_days = 30
        storage_class   = "STANDARD_IA"
      }
    }

    dynamic "noncurrent_version_transition" {
      for_each = var.delete_after_14_days ? [] : [1]
      content {
        noncurrent_days = 60
        storage_class   = "GLACIER"
      }
    }

    dynamic "noncurrent_version_expiration" {
      for_each = var.delete_after_14_days ? [] : [1]
      content {
        noncurrent_days = 90
      }
    }
  }
}

resource "aws_s3_bucket_public_access_block" "this" {
  count  = var.acl == "private" ? 1 : 0
  bucket = aws_s3_bucket.this.id

  block_public_acls       = true
  block_public_policy     = true
  restrict_public_buckets = true
  ignore_public_acls      = true
}

resource "aws_s3_bucket_public_access_block" "public_read_this" {
  count  = var.acl == "public-read" ? 1 : 0
  bucket = aws_s3_bucket.this.id

  block_public_acls       = false
  block_public_policy     = false
  restrict_public_buckets = false
  ignore_public_acls      = false
}

resource "aws_s3_bucket_ownership_controls" "this" {
  count  = var.acl == "public-read" ? 1 : 0
  bucket = aws_s3_bucket.this.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}
