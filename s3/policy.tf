resource "aws_iam_policy" "s3_full_access" {
  name   = "${aws_s3_bucket.this.bucket}_s3_full_access"
  policy = data.aws_iam_policy_document.s3_full_access.json
  tags   = var.additional_tags
}

resource "aws_iam_policy" "s3_ro_access" {
  name   = "${aws_s3_bucket.this.bucket}_s3_ro_access"
  policy = data.aws_iam_policy_document.s3_ro_access.json
  tags   = var.additional_tags
}
