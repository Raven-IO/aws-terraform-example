resource "aws_iam_role_policy_attachment" "lambda_vpc_access_exe_role" {
  role       = module.lambda.lambda_role_name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}
