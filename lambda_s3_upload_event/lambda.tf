module "lambda" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "${replace(var.prefix, "-", "_")}_lambda_s3"

  create_package         = false
  image_uri              = var.docker_image_uri
  package_type           = "Image"
  tags                   = var.additional_tags
  cloudwatch_logs_tags   = var.additional_tags
  role_tags              = var.additional_tags
  s3_object_tags         = var.additional_tags
  vpc_security_group_ids = [
    aws_security_group.lambda.id
  ]
  vpc_subnet_ids        = var.subnet_ids
  environment_variables = {
    "TRACES_QUEUE_NAME"  = "TRACES_DATA"
    "SAMPLES_QUEUE_NAME" = "SAMPLES_DATA"
    "BATCH_PROCESSING"   = "true"
    "REDIS_HOST"         = var.redis_config.host
    "REDIS_PORT"         = var.redis_config.port
  }

  depends_on = [
    aws_security_group.lambda
  ]
}
