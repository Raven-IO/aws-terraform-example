output "lambda_arn" {
  value = module.lambda.lambda_function_arn
}

output "lambda_sg_id" {
  value = aws_security_group.lambda.id
}
