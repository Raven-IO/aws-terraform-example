variable "prefix" {
  type        = string
  description = "The prefix to use for the resources."
}

variable "docker_image_uri" {
  type        = string
  description = "The URI of the Docker image to use for the Lambda function."
}

variable "additional_tags" {
  description = "(Optional) Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  type        = map(string)
  default     = {}
}

variable "redis_config" {
  description = "Redis configuration parameters."
  type = object({
    host = string
    port = string
  })
}

variable "vpc_id" {
  type        = string
  description = "The VPC ID to deploy the lambda function."
}

variable "subnet_ids" {
  type        = list(string)
  description = "The subnet IDs to deploy the lambda function."
}
