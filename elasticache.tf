module "redis" {
  source    = "./elasticache"
  name      = "${var.prefix}-redis"
  vpc_id    = var.vpc_id
  node_type = "cache.t4g.small"

  redis_subnet_group_name = var.elasticache_subnet_group_name
  allowed_security_groups = concat(var.allowed_security_groups, [module.lambda_s3_data_bucket.lambda_sg_id])
  allowed_cidr_blocks     = var.allowed_cidr_blocks
  additional_tags         = var.additional_tags
}
