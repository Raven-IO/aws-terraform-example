variable "private_subnets" {
  type        = list(string)
  description = "The private subnets to deploy the EC2."
}

variable "vpc_id" {
  type        = string
  description = "The VPC ID to deploy the resources."
}

variable "additional_tags" {
  default = {
    "Terraform" = "true"
  }
  description = "Additional tags to attach to the resources."
}

variable "elasticache_subnet_group_name" {
  type        = string
  description = "The name of the Elasticache subnet group to use for the Elasticache (Redis) instance."
}

variable "allowed_cidr_blocks" {
  type        = list(string)
  description = "The CIDR blocks that are allowed to access the RDS (Postgres) / Elasticache (Redis) / EC2 (ClickHouse) instance."
}

variable "allowed_security_groups" {
  type        = list(string)
  description = "The security groups that are allowed to access the RDS (Postgres) / Elasticache (Redis) instance."
}

variable "db_subnet_group_name" {
  type        = string
  description = "The name of the RDS subnet group to use for the RDS (Postgres) instance."
}

variable "docker_image_uri" {
  type        = string
  description = "The URI of the Docker image to use for the Lambda function | Your AWS account should have access to this image."
}

variable "prefix" {
  type        = string
  description = "The prefix to use for the resources."
}
