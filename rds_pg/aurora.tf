resource "random_password" "aurora_password" {
  length  = 16
  special = false
}

module "aurora" {
  source                                = "terraform-aws-modules/rds-aurora/aws"
  version                               = "7.6.0"
  engine_version                        = "14.5"
  storage_encrypted                     = true
  engine_mode                           = "provisioned"
  name                                  = "${replace(var.name, "_", "-")}-postgresql"
  database_name                         = replace(var.name, "-", "")
  engine                                = "aurora-postgresql"
  vpc_id                                = var.vpc_id
  create_db_subnet_group                = false
  db_subnet_group_name                  = var.db_subnet_group_name
  create_security_group                 = true
  allowed_security_groups               = var.allowed_security_groups
  allowed_cidr_blocks                   = var.allowed_cidr_blocks
  performance_insights_enabled          = true
  performance_insights_retention_period = 7
  monitoring_interval                   = 60
  security_group_egress_rules = {
    "all" = {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  serverlessv2_scaling_configuration = {
    min_capacity = var.min_acu
    max_capacity = var.max_acu
  }

  instances = {
    one = {}
  }

  instance_class = "db.serverless"

  master_password         = random_password.aurora_password.result
  create_random_password  = false
  apply_immediately       = true
  skip_final_snapshot     = true
  kms_key_id              = aws_kms_key.aurora.arn
  backup_retention_period = var.backup_retention_period

  create_db_cluster_parameter_group = false
  db_cluster_parameter_group_name   = "default.aurora-postgresql14"

  create_db_parameter_group = false
  db_parameter_group_name   = "default.aurora-postgresql14"

  enabled_cloudwatch_logs_exports = ["postgresql"]
  tags                            = var.additional_tags
}
