resource "aws_kms_key" "aurora" {
  description             = "Database Encryption Key for ${var.name}"
  deletion_window_in_days = 10 # Duration in days after which the key is deleted after destruction of the resource
  enable_key_rotation     = true
  tags                    = var.additional_tags
}
