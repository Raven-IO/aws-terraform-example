resource "aws_secretsmanager_secret" "aurora" {
  name        = "database/${var.name}"
  description = "Database credentials for ${var.name}"
  tags        = var.additional_tags
}

resource "aws_secretsmanager_secret_version" "aurora" {
  secret_id = aws_secretsmanager_secret.aurora.id
  secret_string = jsonencode({
    pg_host : module.aurora.cluster_endpoint,
    pg_user : module.aurora.cluster_master_username,
    pg_port : module.aurora.cluster_port,
    pg_password : random_password.aurora_password.result
  })
}
