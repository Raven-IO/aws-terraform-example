variable "name" {
  description = "The name of the RDS instance or prefix for the resources."
  type        = string
}
variable "vpc_id" {
  description = "The VPC ID to deploy the RDS instance."
  type        = string
}
variable "allowed_cidr_blocks" {
  description = "The CIDR blocks that are allowed to access the RDS instance."
  type        = list(string)
}
variable "backup_retention_period" {
  description = "The days to retain backups for."
  type        = number
}
variable "allowed_security_groups" {
  description = "The security groups that are allowed to access the RDS instance."
  type        = list(string)
}
variable "db_subnet_group_name" {
  description = "The name of the DB subnet group."
  type        = string
}
variable "min_acu" {
  default     = "0.5"
  description = "The minimum Aurora Capacity Units (ACUs) to use."
}
variable "max_acu" {
  default     = "8"
  description = "The maximum Aurora Capacity Units (ACUs) to use."
}

variable "additional_tags" {
  description = "(Optional) Key-value map of resource tags. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  type        = map(string)
  default     = {}
}
