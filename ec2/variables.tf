variable "name" {
  type        = string
  description = "The name of the EC2 instance or prefix for the resources."
}
variable "vpc_id" {
  type        = string
  description = "The VPC ID to deploy the resources."
}
variable "subnet_id" {
  type        = string
  description = "The subnet ID to deploy the EC2."
}
variable "whitelisted_cidr_blocks" {
  type        = list(string)
  description = "The CIDR blocks that are allowed to access the EC2 instance."
}
variable "instance_type" {
  default     = "t2.nano"
  description = "The instance type to deploy the EC2."
}
variable "disk_size" {
  default     = 100
  description = "The disk size in GB for the EC2 instance."
}

variable "ami" {
  default     = ""
  description = "The AMI ID to use for the EC2 instance."
}

variable "iam_instance_profile" {
  default     = null
  description = "The IAM instance profile to attach to the EC2 instance."
}

variable "whitelisted_tcp_ports" {
  default     = [22]
  description = "The TCP ports that are allowed to access the EC2 instance."
}
