resource "aws_secretsmanager_secret" "instance" {
  name = "${var.name}-ssh-key"
}

resource "aws_secretsmanager_secret_version" "instance" {
  secret_id     = aws_secretsmanager_secret.instance.id
  secret_string = tls_private_key.instance.private_key_pem
}
