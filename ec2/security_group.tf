resource "aws_security_group" "instance" {
  name_prefix = var.name
  vpc_id      = var.vpc_id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group_rule" "tcp" {
  count             = length(var.whitelisted_tcp_ports)
  from_port         = var.whitelisted_tcp_ports[count.index]
  protocol          = "TCP"
  security_group_id = aws_security_group.instance.id
  to_port           = var.whitelisted_tcp_ports[count.index]
  type              = "ingress"
  cidr_blocks       = var.whitelisted_cidr_blocks
}
