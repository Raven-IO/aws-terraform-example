resource "aws_instance" "instance" {
  ami                  = var.ami == "" ? data.aws_ami.ubuntu-2204.id : var.ami
  instance_type        = var.instance_type
  key_name             = aws_key_pair.instance.key_name
  iam_instance_profile = var.iam_instance_profile

  subnet_id = var.subnet_id
  vpc_security_group_ids = [
    aws_security_group.instance.id
  ]
  associate_public_ip_address = true

  root_block_device {
    volume_size = var.disk_size
    volume_type = "gp3"
  }

  tags = merge(data.aws_default_tags.current.tags, {
    Name = var.name
  })

  volume_tags = merge(data.aws_default_tags.current.tags, {
    Name = var.name
  })
}
