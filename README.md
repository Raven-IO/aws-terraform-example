<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 5.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 5.49.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_clickhouse"></a> [clickhouse](#module\_clickhouse) | ./ec2 | n/a |
| <a name="module_lambda_s3_data_bucket"></a> [lambda\_s3\_data\_bucket](#module\_lambda\_s3\_data\_bucket) | ./lambda_s3_upload_event | n/a |
| <a name="module_postgres"></a> [postgres](#module\_postgres) | ./rds_pg | n/a |
| <a name="module_redis"></a> [redis](#module\_redis) | ./elasticache | n/a |
| <a name="module_s3_data_bucket"></a> [s3\_data\_bucket](#module\_s3\_data\_bucket) | ./s3 | n/a |
| <a name="module_s3_persist_bucket"></a> [s3\_persist\_bucket](#module\_s3\_persist\_bucket) | ./s3 | n/a |
| <a name="module_s3_storage_bucket"></a> [s3\_storage\_bucket](#module\_s3\_storage\_bucket) | ./s3 | n/a |
| <a name="module_s3_symbols_bucket"></a> [s3\_symbols\_bucket](#module\_s3\_symbols\_bucket) | ./s3 | n/a |
| <a name="module_s3_traces_bucket"></a> [s3\_traces\_bucket](#module\_s3\_traces\_bucket) | ./s3 | n/a |

## Resources

| Name | Type |
|------|------|
| [aws_iam_instance_profile.clickhouse_instance_profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.clickhouse_instance_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_additional_tags"></a> [additional\_tags](#input\_additional\_tags) | Additional tags to attach to the resources. | `map` | <pre>{<br>  "Terraform": "true"<br>}</pre> | no |
| <a name="input_allowed_cidr_blocks"></a> [allowed\_cidr\_blocks](#input\_allowed\_cidr\_blocks) | The CIDR blocks that are allowed to access the RDS (Postgres) / Elasticache (Redis) / EC2 (ClickHouse) instance. | `list(string)` | n/a | yes |
| <a name="input_allowed_security_groups"></a> [allowed\_security\_groups](#input\_allowed\_security\_groups) | The security groups that are allowed to access the RDS (Postgres) / Elasticache (Redis) instance. | `list(string)` | n/a | yes |
| <a name="input_db_subnet_group_name"></a> [db\_subnet\_group\_name](#input\_db\_subnet\_group\_name) | The name of the RDS subnet group to use for the RDS (Postgres) instance. | `string` | n/a | yes |
| <a name="input_docker_image_uri"></a> [docker\_image\_uri](#input\_docker\_image\_uri) | The URI of the Docker image to use for the Lambda function \| Your AWS account should have access to this image. | `string` | n/a | yes |
| <a name="input_elasticache_subnet_group_name"></a> [elasticache\_subnet\_group\_name](#input\_elasticache\_subnet\_group\_name) | The name of the Elasticache subnet group to use for the Elasticache (Redis) instance. | `string` | n/a | yes |
| <a name="input_prefix"></a> [prefix](#input\_prefix) | The prefix to use for the resources. | `string` | n/a | yes |
| <a name="input_private_subnets"></a> [private\_subnets](#input\_private\_subnets) | The private subnets to deploy the EC2. | `list(string)` | n/a | yes |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The VPC ID to deploy the resources. | `string` | n/a | yes |

## Outputs

No outputs.
<!-- END_TF_DOCS -->