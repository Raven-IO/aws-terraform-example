module "lambda_s3_data_bucket" {
  source           = "./lambda_s3_upload_event"
  prefix           = "${var.prefix}-nodes-data"
  docker_image_uri = var.docker_image_uri
  additional_tags  = var.additional_tags
  redis_config = {
    host = module.redis.redis_host
    port = module.redis.redis_port
  }
  vpc_id     = var.vpc_id
  subnet_ids = var.private_subnets
}
