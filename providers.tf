terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 5.0.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"

  default_tags {
    tags = {
      PROVISIONER = "TERRAFORM"
      GIT_REPO    = "https://gitlab.com/Raven-IO/aws-terraform-example"
      PRODUCT     = "RAVEN PLATFORM"
    }
  }
}
